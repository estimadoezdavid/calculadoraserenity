/*
 * Copyright (c) 2021
 * David Moreno Hernandez - Test automation
 * https://www.linkedin.com/public-profile/in/davidh-morenoh/
 *
 * All rights reserved Date: 08/01/21, 7:28 p. m.
 */

package com.automation.qa.calculator.tasks;

import com.automation.qa.calculator.interactions.PushKey;
import com.automation.qa.calculator.interactions.SelectOperation;
import com.automation.qa.calculator.user_interfaces.CalculatorPage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;

/**
 * @author <a href="davidh.morenoh@outlook.com">David Moreno Hernandez</a>
 * @version 1.0.0
 * @since 1.0.0
 **/
public class DoOperation implements Task {

    private String operation;
    private String numberOne;
    private String numberTwo;

    public DoOperation(String operation) {
        this.operation = operation;
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(PushKey.number(numberOne), SelectOperation.arithmetic(operation), PushKey.number(numberTwo),
                Click.on(CalculatorPage.EQUAL));
    }

    public static DoOperation arithmetic(String operation) {
        return Tasks.instrumented(DoOperation.class, operation);
    }

    public DoOperation withTwoNumbers(String numberOne, String numberTwo) {
        this.numberOne = numberOne;
        this.numberTwo = numberTwo;
        return this;
    }

}