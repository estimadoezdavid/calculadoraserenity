/*
 * Copyright (c) 2021
 * David Moreno Hernandez - Test automation
 * https://www.linkedin.com/public-profile/in/davidh-morenoh/
 *
 * All rights reserved Date: 08/01/21, 7:20 p. m.
 */

package com.automation.qa.calculator.interactions;

import com.automation.qa.calculator.user_interfaces.CalculatorPage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Interaction;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;

/**
 * @author <a href="davidh.morenoh@outlook.com">David Moreno Hernandez</a>
 * @version 1.0.0
 * @since 1.0.0
 **/
public class SelectOperation implements Interaction {

    private String operation;

    public SelectOperation(String operation) {
        this.operation = operation;
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        switch (operation) {
            case "division":
                actor.attemptsTo(Click.on(CalculatorPage.OPERATION.of("dividir")));
                break;
            case "multiplicacion":
                actor.attemptsTo(Click.on(CalculatorPage.OPERATION.of("multiplicar")));
                break;
            case "resta":
                actor.attemptsTo(Click.on(CalculatorPage.OPERATION.of("menos")));
                break;
            case "suma":
                actor.attemptsTo(Click.on(CalculatorPage.OPERATION.of("más")));
                break;
            default:
                break;
        }
    }

    public static SelectOperation arithmetic(String operation) {
        return Tasks.instrumented(SelectOperation.class, operation);
    }

}